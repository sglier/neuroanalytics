setwd("~/hapmap")
#reading in all of the datasets
ped = read.table("hapmap1_nomissing_AF.ped")
map = read.table("hapmap1_nomissing_AF.map")
qt =  read.table("qt.phe")


#taking only the subset of columns containing SNP information I need from map file
map <- map[,c(2,4)]
colnames(map)[1] <- "SNPid" #renaming col 1 into snpID
map <- map[c(1:100),] #just need to plot the first 100 SNPs
colnames(qt)[3] <- "phenotype" #rename col 3 into phenotype

#getting rid of the subject descriptors in first 6 columns; every 2 columns corresponds to a genotype @ a specific SNP
ped2 <- ped[,c(7:154644)]
ped3 <- ped2[,c(1:200)] #only want to plot the 1st 100 SNPs
#now need to sum every pair (7+8), (9+10), etc into new dataframe 
#new dataframe called ped3 will be 89 rows and 83,534 col long
id <- 1:ncol(ped3)
ped4 <- ped3[ , id[id%%2!=0] ] + ped3[ , id[id%%2==0] ]

#now I need to recode all of the genotypes
#0 = missing, going to code as NA
#1+1 = 2 = homozygous allele 1 = homozyg1
#1+2 = 3 = heterozygous 
#2+2 = 4 = homozygous allele 2 = homozyg2

#forgot we only had to plot the 1st 100 SNPs so taking only those from the ped3 dataframe I made earlier
ped4[ped4==0]<-NA 
ped4[ped4==2]<-"homozygous_allele1"
ped4[ped4==3]<-"heterozygous"
ped4[ped4==4]<-"homozygous_allele2"

library(data.table)
#transposing my dataframe - in the longrun I didn't actually need to do this but I was playing around with different ways to combine and manipulate my dataframes
map2_transpose <- transpose(map)
map2_transpose <- map2_transpose[c(-2),] #getting rid of extra row, just want rs-IDs

qt2<- qt[,c(-2)] #getting rid of the extraneous column here which doesn't contain ID or pheno

#now each genotype is named for its SNP name "rs###"
names(ped4) <- map2_transpose

#now I have a wide dataset with each genotype nameby by its SNP in its own column and the corresponding phenotypes
qt_ped <- cbind(qt2, ped4) #there were faster ways to combine all of these and drop /subset all the columns I wanted at once, but was manipulating data. This is essentially binding together my phenotype and genotypes.
qt_ped <- qt_ped[,c(-1)] #getting rid of 1st participant ID col

##again as in the last hw I want to transform data to long
library(tidyverse)
library(tidyr)
library(ggplot2)


#transforming to long here - making every column except the phenotype long by subsetting column names into a new "key" column called "SNPid" and all their respective values (coded strings) into a column called genotype
      #df <- ped4 %>% pivot_longer(cols = starts_with("rs"), names_to='SNPid', values_to='genotype') #forgot the phenotype here went back to grab - this will make long data sans phenotype
df <- gather(qt_ped,
             key = "SNPid",
             value = "genotype",
             -phenotype)

df2 <- na.omit(df) #ggplot can handle NA values, but for ease going to just remove them all

#THIS will allow me to plot in order 
level_order <- factor(df2$genotype, level = c('homozygous_allele1', 'heterozygous', 'homozygous_allele2'))

#below was all test plotting kept for reference
#l <- ggplot(data = df2, aes(x = genotype, y = phenotype)) +   
#  geom_point() +
#  xlab("genotype") + ylab("phenotype") +   
#  facet_wrap(~SNPid)
#l


library(ggforce)
# this package comes with facet_wrap_paginate which will allow me to wrap each donor's plot onto their own page within the pdf
ggplot(data = df2, aes(x = level_order, y = phenotype)) +   
  geom_point() +
  xlab("genotype") + ylab("phenotype") +   
  facet_wrap_paginate(~SNPid,
                      nrow = 1, 
                      ncol = 1, 
                      scales = "free") +
  theme( strip.text = element_text(size = 30)) -> q  #I am saving this into a variable q
required_n_pages <- n_pages(q) #this will now tell me how many pages I need to specify for new pdf
q


# this will now allow me to plot over multiple pages.  Variation of what we did above with facet wrap but now using facet_paginate from the ggforce package

pdf("genotypeassociation_filtered.pdf")
for(i in 1:required_n_pages){
  ggplot(data = df2, aes(x = level_order, y = phenotype)) +   
    geom_point() +
    xlab("genotype") + ylab("phenotype") +  
    facet_wrap_paginate(~SNPid,
                        nrow = 1, 
                        ncol = 1, 
                        scales = "free",
                        page = i) +
    theme( strip.text = element_text(size = 30)) -> r
  
  print(r)
}
dev.off()
