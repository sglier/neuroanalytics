setwd("~/hapmap")
#reading in all of the datasets
ped = read.table("hapmap1.ped")
map = read.table("hapmap1.map")
qt =  read.table("qt.phe")

colnames(map)[2] <- "SNPid" #renaming col 2 into snpID
colnames(qt)[3] <- "phenotype" #rename col 3 into phenotype
colnames(qt)[1] <- "ID"


ped2 <- ped[,c(7:167074)] #getting rid of the subject descriptors in first 6 columns; every 2 columns corresponds to a genotype @ a specific SNP

#now need to sum every pair (7+8), (9+10), etc into new dataframe; new dataframe called ped3 will be 89 rows and 83,534 col long
id <- 1:ncol(ped2)
ped3 <- ped2[ , id[id%%2!=0] ] + ped2[ , id[id%%2==0] ]

map2 <- map[,c(2,4)]

library(tidyverse)
map3 <- map2 %>%
  pivot_wider(
    names_from = SNPid,
    values_from = V4
  )

colnames(ped3) <- colnames(map3) #now my dataframe has all the rs as colnames for each genotype

ped3[ped3==0]<-NA #subsetting those 0 / missing genotypes as NAs - makes the subsetting easier later as R has built in functions for NA values


# loop through columns and reject SNPs with >5% having 0 0 as their genotype (AKA since we are working with 100 SNPs if there are 5 individuals missing/0 as genotype we will remove)
# store those snp names in a separate dataframe

QCid <- names(which(colMeans(is.na(ped3)) > 0.05)) #identifies those SNPs w/ >5% NA and writes it to list called QCid
write.table(QCid,"rsIDs.txt",sep="\t",row.names=FALSE, col.names=FALSE) #writing it to a txt file as a part of the HW

      # QC <- test[, -which(colMeans(is.na(test)) > 0.05)] #using same sort of code above except now I am using (-)which to exclude those columns with >5% NA and writing to a new QC dataframe


#forgot genotypes should be in same format as original going back to orig file from earlier

ped2[ped2==0]<-NA
QC.ped <- ped2[, -which(colMeans(is.na(ped2)) > 0.05)]
QC.ped[is.na(QC.ped)] <- 0 #recoding those NAs back to zeros to match the original dataframe
ped.info <- ped[,c(1:6)] #original info from original ped file
QC.ped.2 <- cbind(ped.info, QC.ped) #combining subject info back to genotype info from original ped
#QC.ped.3 <- unname(QC.ped.2) #OLD PART OF CODE: getting rid of the SNP names so it will write in the same format as original ped file
write.table(QC.ped.2,"hapmap1_nomissing.ped",row.names=FALSE, col.names=FALSE)


#getting the list of rs that have high missingness and changing them into a dataframe
QCid <- as.matrix(QCid)
colnames(QCid)[1] <- "SNPid"

#https://stackoverflow.com/questions/17338411/delete-rows-that-exist-in-another-data-frame
map_remove <- map[!(map$SNPid %in% QCid),] #getting rid of row names that match between these
map_remove.1 <- unname(map_remove) #getting rid of the SNP names so it will write in the same format as original ped file
write.table(map_remove.1,"hapmap1_nomissing.map",row.names=FALSE, col.names=FALSE)



