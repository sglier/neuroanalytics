setwd("~/hapmap")
#reading in all of the datasets
ped = read.table("hapmap1.ped")
map = read.table("hapmap1.map")
qt =  read.table("qt.phe")

#taking only the subset of columns containing SNP information I need from map file
map <- map[,c(2,4)]
colnames(map)[1] <- "SNPid" #renaming col 1 into snpID
colnames(qt)[3] <- "phenotype" #rename col 3 into phenotype

#getting rid of the subject descriptors in first 6 columns
#every 2 columns corresponds to a genotype @ a specific SNP
ped2 <- ped[,c(7:167074)]
#now need to sum every pair (7+8), (9+10), etc into new dataframe 
#new dataframe called ped3 will be 89 rows and 83,534 col long
id <- 1:ncol(ped2)
ped3 <- ped2[ , id[id%%2!=0] ] + ped2[ , id[id%%2==0] ]

#now I need to recode all of the genotypes
#0 = missing, going to code as NA
#1+1 = 2 = homozygous allele 1 = homozyg1
#1+2 = 3 = heterozygous 
#2+2 = 4 = homozygous allele 2 = homozyg2

#forgot we only had to plot the 1st 100 SNPs so taking only those from the ped3 dataframe I made earlier
test = ped3[,c(1:100)]
map2 = map[c(1:100),]
test[test==0]<-NA
test[test==2]<-"homozyg1"
test[test==3]<-"heterozygous"
test[test==4]<-"homozyg2"

library(data.table)
#transposing my dataframe - in the longrun I didn't actually need to do this but I was playing around with different ways to combine and manipulate my dataframes
map2_transpose <- transpose(map2)
map2_transpose <- map2_transpose[c(-2),] #getting rid of extra row, just want rs-IDs

#now each genotype is named for its SNP name "rs###"
names(test) <- map2_transpose

qt2<- qt[,c(-2)] #getting rid of the extraneous column here which doesn't contain ID or pheno
#now I have a wide dataset with each genotype nameby by its SNP in its own column and the corresponding phenotypes
qt_ped <- cbind(qt2, test) #there were faster ways to combine all of these and drop /subset all the columns I wanted at once, but was manipulating data. This is essentially binding together my phenotype and genotypes.

##again as in the last hw I want to transform data to long
library(tidyverse)
library(tidyr)
library(ggplot2)

qt_ped2 <- qt_ped[,c(-1)] #getting rid of 1st participant ID col

#transforming to long here - making every column except the phenotype long by subsetting column names into a new "key" column called "SNPid" and all their respective values (coded strings) into a column called genotype
df <- gather(qt_ped2,
                   key = "SNPid",
                   value = "genotype",
                   -phenotype)

df2 <- na.omit(df) #ggplot can handle NA values, but for ease going to just remove them all

#THIS will allow me to plot in order 
level_order <- factor(df2$genotype, level = c('homozyg1', 'heterozygous', 'homozyg2'))

#below was all test plotting kept for reference
#l <- ggplot(data = df2, aes(x = genotype, y = phenotype)) +   
#  geom_point() +
#  xlab("genotype") + ylab("phenotype") +   
#  facet_wrap(~SNPid)
#l


library(ggforce)
# this package comes with facet_wrap_paginate which will allow me to wrap each donor's plot onto their own page within the pdf
ggplot(data = df2, aes(x = level_order, y = phenotype)) +   
  geom_point() +
  xlab("genotype") + ylab("phenotype") +   
  facet_wrap_paginate(~SNPid,
                      nrow = 1, 
                      ncol = 1, 
                      scales = "free") +
  theme( strip.text = element_text(size = 30)) -> q  #I am saving this into a variable q
required_n_pages <- n_pages(q) #this will now tell me how many pages I need to specify for new pdf
q


# this will now allow me to plot over multiple pages.  Variation of what we did above with facet wrap but now using facet_paginate from the ggforce package

pdf("genotypeassociation.pdf")
for(i in 1:required_n_pages){
  ggplot(data = df2, aes(x = level_order, y = phenotype)) +   
    geom_point() +
    xlab("genotype") + ylab("phenotype") +  
    facet_wrap_paginate(~SNPid,
                        nrow = 1, 
                        ncol = 1, 
                        scales = "free",
                        page = i) +
    theme( strip.text = element_text(size = 30)) -> r
  
  print(r)
}
dev.off()



