#!/bin/bash
#sarah glier homework 1
#using the manual found each individual call for these
# echo = display line of text following and by default this command will cause a line break at end of output
# echo -n: do not output trailing newline so script flashes on screen without making new line each time
# echo -e: enable interpretation of backslash escapes (or these: \) 
# IF echo -e is in effect then \b will act as a backspace where the cursor will move physically backward on the screen one space and \\ is actually one backslash and \n acts to signal script to make a new line


##PART A of HOMEWORK1
echo -ne '/'; # prints /
sleep 1; # tells script to wait 1 sec before printing next line of code
echo -ne '\b-'; # as noted above when \b follows -e then acts as backspace, does NOT print on screen, only prints -
sleep 1;
echo -ne '\b\\'; # as noted above following -e \\ is interpreted as backslash so code prints \
sleep 1;
echo -ne '\b|'; # cursor move back a space (\b) and prints |
sleep 1;
echo -ne '\b/'; # backspace and prints /
sleep 1;
echo -ne '\b-'; # backspace and prints -
sleep 1;
echo -ne '\b\\'; # backspace and prints \
sleep 1;
echo -ne '\b|'; # backspace and prints |
#the use of " " instead of ' ' indicates that script should return VALUES of variables and commands instead of when they are inside of ' ' which indicates that everything should be treated literally or exactly
#additionally the \retains special meaning within "" ONLY  when it is followed by another \ or other select characters and newline.  
echo -ne "\b \n";#cursor moves back left, but then we have an escape within "" indicated by the two \\ sequentially so it will print a backslash and then the \n indicates script to create a newline before completing.

#PART B of HW 1 making it turn in different direction
echo -ne "\b \n"; 
sleep 0.1; #same script as above except now I'm telling it to only pause between commands by 1/10th of a second
echo -ne '\b-';
sleep 0.1 
echo -ne '\b/';
sleep 0.1
echo -ne '\b|'; 
sleep 0.1;
echo -ne '\b-';
sleep 0.1 
echo -ne '\b/';
echo -ne '\b|'; 
sleep 0.1
echo -ne '\b|';   
echo -ne "\b \n";


##PART C 
echo -ne '/'; 
sleep 0.1; #1/10th of a second wait before print next line
echo -ne '\b-';
sleep 0.1 
echo -ne '\b\\'; 
sleep 0.1;
echo -ne '\b|';
sleep 0.1;
echo -ne '\b/'; 
sleep 0.1;
echo -ne '\b-'; 
sleep 0.1;
echo -ne '\b\\'; 
sleep 0.1;
echo -ne '\b|';   
echo -ne "\b \n";






#PART D
for x in 1 1 1 1 1 1 1 1 1 1 #executing echo command 10 times with this syntax x in 1 space 10 times -  within loop essentially results in echo commands print 10x - just wanted to see if this would work!
# source http://www.compciv.org/topics/bash/loops/
do
echo -ne '/'; 
sleep 0.1; 
echo -ne '\b-'; 
sleep 0.1;
echo -ne '\b\\'; 
sleep 0.1;
echo -ne '\b|'; # cursor move back a space (\b) and prints |
sleep 0.1;
echo -ne '\b/'; # backspace and prints /
sleep 0.1;
echo -ne '\b-'; # backspace and prints -
sleep 0.1;
echo -ne '\b\\'; # backspace and prints \
sleep 0.1;
echo -ne '\b|';   
echo -ne "\b \n";
done


#PART E 

for x in 1 1 1 1 1 1 1 1 1 1 
 do
echo -ne '\n'; #I want to start on a new line before starting swirling character below
echo -ne '\bV'; 
sleep 1; 
echo -ne '\b<'; 
sleep 1;
echo -ne '\bA'; # sorry an "A" was the closest character to V < > 
sleep 1;
echo -ne '\b>'; |
sleep 1;
echo -ne '\bV\b';
 done