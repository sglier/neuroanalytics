#!/bin/bash

# wc -l hapmap1.ped #89 lines in datafile
# wc -w hapmap1.ped #14869586 words
# less -S hapmap1.ped #allows me to visualize without the line breaks

value=$(<hapmap1.ped) #allows me to store entire file in a variable I've arbitrarily named value
 
for i in $value #now I want to loop through all lines in dataframe 
do
    tail -n +89 hapmap1.ped | head -n 89 #this is a roundabout way using tail to start at line 89 (the last line) and head to print all 89 lines with a sleep delay between each line added below 
    sleep 1  
done




##IGnore below some failed attempts

# filename='hapmap1.ped'

# wc -l hapmap1.ped #89 lines in datafile
# wc -w hapmap1.ped #14869586 words
#value=`tail -n +89 hapmap1.ped | head -n 89`;

#FS=$'\n'
#for i in $value
#do
#    echo "$value{i}"
#    sleep 0.3 #waits 3/10ths of a second to print each new line
#done
