#!/bin/bash
#The structure is for ...variable... in ...list... ; do ...body... ; done
mkdir fakedata 
 

#find . -name "*.txt" -print0 | xargs -0 -I {} mv {} /fakedata/

for f in `find . -name "*.txt*"`; #for loop, it will find some common name and the "." before the -name is referring to the pwd (current directory) that I am working in. This means find will search through my current directory to find all files containing the ".txt" suffix. This means it will find all of the txt data files that I have previously made in the create data script. 
do
    mv $f ~/hapmap/fileseries/fakedata/; # for "f" - or for every loop / instance of ".txt" that is found, that file will now be moved to my subdirectory that I made earlier called fakedata
done

#for loop to echo create directory for each donor and make new directory for each donor (1-50)
for j in `seq 1 50`
do
    echo "Creating directory: donordirect${j}"; 
    mkdir fakedata/donordirect${j};
done

# nested for loop to search in /fake data directory for files that all have the same common name of "donor" and numbers 1-50.  Then it will move all files with matching donor# into their own respective directory we created above while preserving the filename
for j in `seq 1 50`;
do
    for f in `find ~/hapmap/fileseries/fakedata/ -name "*donor${j}*"`;
    do
	mv $f ~/hapmap/fileseries/fakedata/donordirect${j};
done
done
