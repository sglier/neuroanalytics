#!/bin/bash
#The structure is for ...variable... in ...list... ; do ...body... ; done

#these are nested loops so within the sequence of donors (j loop) it will also loop through a series of 10 in i for the tp/timepoint. Essentially this means for each of the 50 donors it will reloop through and evaluate that same expression 10 times for time points within donors.
for j in `seq 1 50`
do
  for i in `seq 1 10`
  do
    echo "Creating file: donor${j}_tp${i}.txt"; #print to my command line each time it is looping through which file it is making.
    touch donor${j}_tp${i}.txt; #touch means that we are creating a file in the current directory and we are making a file called donor and then calling our sequence number from our loop by subsetting with the $ {j} and WITHIN this loop we then continue to evaluate the expression and loop through the nested {i} sequence. It will not move on in the j sequence until the inner i loop has completed its full sequence
    echo "data" >>  donor${j}_tp${i}.txt; #I am now creating/writing a string called data within our newly created dataframe.  Same nesting loop structure applies here
    echo $(( $RANDOM % 256 )) >>  donor${j}_tp${i}.txt; #this is using the echo command like we did above to write to our dataframe except $RANDOM generates a random number within a specified range. Since I did not put 0 % before random, it will automatically default to 0 as the minimum and I used 256 as an arbitrary maximum. This 256 can be changed to any number to change range of values possible
    echo $(( $RANDOM % 256 )) >>  donor${j}_tp${i}.txt; #sorry I got really lazy and just repeated this to generate 5 random numbers in the column header "data"
    echo $(( $RANDOM % 256 )) >>  donor${j}_tp${i}.txt;
    echo $(( $RANDOM % 256 )) >>  donor${j}_tp${i}.txt;
    echo $(( $RANDOM % 256 )) >>  donor${j}_tp${i}.txt;
 done
done  #both dones are required as we have TWO nested loops and our script needs to be told that both loops are finished.

