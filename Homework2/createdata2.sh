#!/bin/bash

#HW 2 problem number 2
# the curly brackets allow me to essentially replace the `seq` part of the previous script and it will now loop through and make file names with the added zeros as I have specified within the brackets

for donor in {001..100}
do
    for tp in {01..10}
    do

	rand1=$RANDOM
	rand2=$RANDOM
	rand3=$RANDOM
	rand4=$RANDOM
	rand5=$RANDOM
	
	echo "data" > donor${donor}_tp${tp}.txt;
	echo ${rand1} >> donor${donor}_tp${tp}.txt;
	echo ${rand2} >> donor${donor}_tp${tp}.txt;
	echo ${rand3} >> donor${donor}_tp${tp}.txt;
	echo ${rand4} >> donor${donor}_tp${tp}.txt;
	echo ${rand5} >> donor${donor}_tp${tp}.txt;
	
    done
done

